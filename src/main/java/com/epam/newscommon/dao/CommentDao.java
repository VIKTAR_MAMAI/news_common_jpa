package com.epam.newscommon.dao;

import com.epam.newscommon.entity.CommentEntity;

/**
 * 
 * interacts with table comments
 */
public interface CommentDao extends CommonDao<Long, CommentEntity> {
}
