package com.epam.newscommon.dao;

import java.util.List;

import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.valueobject.FilteredItem;

/**
 * 
 * interacts with table news
 * 
 */
public interface NewsDao extends CommonDao<Long, NewsEntity> {

	/**
	 * 
	 * @param filteredItem
	 *            - object which is used as a filter
	 * @param pageNumber
	 *            - number of page with news, displayed on view
	 * @param newsPerPage
	 *            - count of news displayed on one single page
	 * @return - list of news objects
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	List<NewsEntity> loadByFilter(FilteredItem filteredItem,
			Integer pageNumber, int newsPerPage) throws DaoException;

	/**
	 * 
	 * @param filteredItem
	 *            - object which is used as a filter
	 * @param newsId
	 *            - unique news identifier
	 * @return - next news identifier from database according to specified
	 *         filter
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	Long loadNextId(FilteredItem filteredItem, Long newsId) throws DaoException;

	/**
	 * 
	 * @param filteredItem
	 *            - object which is used as a filter
	 * @param newsId
	 *            - unique news identifier
	 * @return - previous news identifier from database according to specified
	 *         filter
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	Long loadPreviousId(FilteredItem filteredItem, Long newsId) throws DaoException;

	/**
	 * 
	 * @param newsIdList
	 *            - array of unique news identifiers
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	void deleteList(List<Long> newsIdList) throws DaoException;

	/**
	 * 
	 * @param filteredItem
	 *            - object which is used as a filter
	 * @return - total count of news entities which matches current filter
	 * @throws DaoException
	 *             - when there is a connection error
	 */
	int count(FilteredItem filteredItem) throws DaoException;
}