package com.epam.newscommon.dao;

import java.util.List;

import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;

/**
 * 
 * interacts with table tag
 */
public interface TagDao extends CommonDao<Long, TagEntity> {
	List<TagEntity> loadByIdList(List<Long> tagIdList) throws DaoException;
}
