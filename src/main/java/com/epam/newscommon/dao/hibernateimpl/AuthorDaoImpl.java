package com.epam.newscommon.dao.hibernateimpl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorEntity;

@Repository
public class AuthorDaoImpl implements AuthorDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<AuthorEntity> loadAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<AuthorEntity> authorList = session.createCriteria(AuthorEntity.class).addOrder(Order.asc("name")).list();
		return authorList;
	}

	@Override
	public List<AuthorEntity> loadActiveAuthors() {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.getNamedQuery("Author.loadActiveAuthors");
		List<AuthorEntity> authorList = query.list();
		return authorList;
	}

	@Override
	public AuthorEntity loadById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		AuthorEntity entity = (AuthorEntity) session.get(AuthorEntity.class, id);
		return entity;
	}

	@Override
	public Long create(AuthorEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		Long id = (Long) session.save(entity);
		return id;

	}

	@Override
	public void update(AuthorEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(entity);
	}

	@Override
	public void delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		AuthorEntity entity = (AuthorEntity) session.get(AuthorEntity.class, id);
		if (entity != null) {
			session.delete(entity);
		}
	}

	@Override
	public void makeExpired(Long authorId) {
		Session session = this.sessionFactory.getCurrentSession();	
		AuthorEntity author= (AuthorEntity) session.get(AuthorEntity.class, authorId);
		author.setExpiredDate(new Date());
		session.update(author);
	}

}
