package com.epam.newscommon.dao.hibernateimpl;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.CommentDao;
import com.epam.newscommon.entity.CommentEntity;

@Repository
public class CommentDaoImpl implements CommentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<CommentEntity> loadAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<CommentEntity> commentList = session.createCriteria(CommentEntity.class).list();
		return commentList;
	}

	@Override
	public CommentEntity loadById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		CommentEntity entity = (CommentEntity) session.get(CommentEntity.class, id);
		return entity;
	}

	@Override
	public Long create(CommentEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		entity.setCreationDate(new Date());
		Long id = (Long) session.save(entity);
		return id;
	}

	@Override
	public void update(CommentEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(entity);
	}

	@Override
	public void delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		CommentEntity entity = (CommentEntity) session.get(CommentEntity.class, id);
		if (entity != null) {
			session.delete(entity);
		}
	}

}
