package com.epam.newscommon.dao.hibernateimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.util.QueryHelper;
import com.epam.newscommon.valueobject.FilteredItem;

@Repository
public class NewsDaoImpl implements NewsDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<NewsEntity> loadAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<NewsEntity> newsList = session.createCriteria(NewsEntity.class)
				.list();
		return newsList;
	}

	@Override
	public NewsEntity loadById(Long id) throws DaoException {
		Session session = this.sessionFactory.getCurrentSession();
		NewsEntity entity = (NewsEntity) session.get(NewsEntity.class, id);
		return entity;
	}

	@Override
	public List<NewsEntity> loadByFilter(FilteredItem filteredItem,
			Integer pageNumber, int newsPerPage) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();
		String strQuery = QueryHelper.loadOrderedList(filteredItem);
		Query query = session.createSQLQuery(strQuery).addScalar("news_id",
				LongType.INSTANCE);
		if (!isAuthorNull) {
			query.setParameter("authorId", authorId);
		}
		if (!isTagNull) {
			query.setParameterList("tagIdList", tagIdList);
		}
		int firstIdx = (pageNumber - 1) * newsPerPage;
		query.setFirstResult(firstIdx);
		query.setMaxResults(newsPerPage);
		List<Long> idList = (List<Long>) query.list();
		List<NewsEntity> newsList = new ArrayList<NewsEntity>();
		if (idList == null || idList.isEmpty()) {
			return newsList;
		}
		for (Long obj : idList) {
			NewsEntity newsEntity = (NewsEntity) session.get(NewsEntity.class,
					obj);
			newsList.add(newsEntity);
		}
		return newsList;
	}

	@Override
	public Long create(NewsEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		entity.setModificationDate(new Date());
		Long id = (Long) session.save(entity);
		return id;
	}

	@Override
	public void update(NewsEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(entity);
	}


	@Override
	public Long loadNextId(FilteredItem filteredItem, Long id)
			throws DaoException {
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();

		Session session = this.sessionFactory.getCurrentSession();
		String strQuery = QueryHelper.loadOffsetId(filteredItem, 1);
		Query query = session.createSQLQuery(strQuery).addScalar("news_id",
				LongType.INSTANCE);
		if (!isAuthorNull) {
			query.setParameter("authorId", authorId);
		}
		if (!isTagNull) {
			query.setParameterList("tagIdList", tagIdList);
		}
		query.setParameter("newsId", id);
		Long nextId = (Long) query.uniqueResult();
		return nextId;
	}

	@Override
	public Long loadPreviousId(FilteredItem filteredItem, Long id)
			throws DaoException {
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();

		Session session = this.sessionFactory.getCurrentSession();
		String strQuery = QueryHelper.loadOffsetId(filteredItem, -1);
		Query query = session.createSQLQuery(strQuery).addScalar("news_id",
				LongType.INSTANCE);
		if (!isAuthorNull) {
			query.setParameter("authorId", authorId);
		}
		if (!isTagNull) {
			query.setParameterList("tagIdList", tagIdList);
		}
		query.setParameter("newsId", id);
		Long previousId = (Long) query.uniqueResult();
		return previousId;
	}

	@Override
	public void delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		NewsEntity entity = (NewsEntity) session.get(NewsEntity.class, id);
		if (entity != null) {
			session.delete(entity);
		}
	}

	@Override
	public void deleteList(List<Long> newsIdList) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.getNamedQuery("News.deleteList");
		query.setParameterList("idList", newsIdList);
		query.executeUpdate();
	}

	@Override
	public int count(FilteredItem filteredItem) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();
		Criteria cr = session.createCriteria(NewsEntity.class, "news");
		if (!isAuthorNull) {
			cr.setFetchMode("news.author", FetchMode.JOIN);
			cr.createAlias("news.author", "authors");
			cr.add(Restrictions.eq("authors.id", authorId));
		}
		if (!isTagNull) {
			cr.setFetchMode("news.tags", FetchMode.JOIN);
			cr.createAlias("news.tags", "tags");
			cr.add(Restrictions.in("tags.id", tagIdList));
		}
		cr.setProjection(Projections.countDistinct("news.id"));
		int count = ((Long) cr.uniqueResult()).intValue();
		return count;
	}
}
