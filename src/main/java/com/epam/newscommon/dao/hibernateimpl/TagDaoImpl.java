package com.epam.newscommon.dao.hibernateimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.TagDao;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;

@Repository
public class TagDaoImpl implements TagDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<TagEntity> loadAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<TagEntity> tagList = session.createCriteria(TagEntity.class)
				.addOrder(Order.asc("name")).list();
		return tagList;
	}

	@Override
	public TagEntity loadById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		TagEntity entity = (TagEntity) session.get(TagEntity.class, id);
		return entity;
	}

	@Override
	public List<TagEntity> loadByIdList(List<Long> tagIdList)
			throws DaoException {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.getNamedQuery("Tag.loadByIdList");
		query.setParameterList("tagIdList", tagIdList);
		List<TagEntity> tagList = query.list();
		return tagList;
	}

	@Override
	public Long create(TagEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		Long id = (Long) session.save(entity);
		return id;
	}

	@Override
	public void update(TagEntity entity) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(entity);
	}

	@Override
	public void delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		TagEntity entity = (TagEntity) session.get(TagEntity.class, id);
		if (entity != null) {
			session.delete(entity);
		}
	}

}
