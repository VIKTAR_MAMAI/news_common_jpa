package com.epam.newscommon.dao.jpaimpl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.exception.DaoException;

@Repository
public class AuthorDaoImpl implements AuthorDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<AuthorEntity> loadAll() throws DaoException {
		Query query = entityManager.createQuery("from AuthorEntity order by name asc");
		return query.getResultList();
	}

	@Override
	public AuthorEntity loadById(Long id) throws DaoException {
		AuthorEntity author = entityManager.find(AuthorEntity.class, id);
		return author;
	}

	@Override
	public Long create(AuthorEntity entity) throws DaoException {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getId();
	}

	@Override
	public void update(AuthorEntity entity) throws DaoException {
		entityManager.merge(entity);
	}

	@Override
	public void delete(Long id) throws DaoException {
		AuthorEntity entity = loadById(id);
		if (entity != null) {
			entityManager.remove(entity);
		}
	}

	@Override
	public void makeExpired(Long authorId) throws DaoException {
		AuthorEntity author = entityManager.find(AuthorEntity.class, authorId);
		author.setExpiredDate(new Date());
		entityManager.merge(author);
	}

	@Override
	public List<AuthorEntity> loadActiveAuthors() throws DaoException {
		Query query = entityManager.createNamedQuery("Author.loadActiveAuthors");
		return query.getResultList();
	}

}
