package com.epam.newscommon.dao.jpaimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.epam.newscommon.dao.CommentDao;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.exception.DaoException;

@Repository
public class CommentDaoImpl implements CommentDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<CommentEntity> loadAll() throws DaoException {
		Query query = entityManager.createQuery("from CommentEntity");
		return query.getResultList();
	}

	@Override
	public CommentEntity loadById(Long id) throws DaoException {
		CommentEntity comment = entityManager.find(CommentEntity.class, id);
		return comment;
	}

	@Override
	public Long create(CommentEntity entity) throws DaoException {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getId();
	}

	@Override
	public void update(CommentEntity entity) throws DaoException {
		entityManager.merge(entity);
	}

	@Override
	public void delete(Long id) throws DaoException {
		CommentEntity entity = loadById(id);
		if (entity != null) {
			entityManager.remove(entity);
		}
	}

}
