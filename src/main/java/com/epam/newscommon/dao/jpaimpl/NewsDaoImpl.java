package com.epam.newscommon.dao.jpaimpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.util.QueryHelper;
import com.epam.newscommon.valueobject.FilteredItem;

@Repository
public class NewsDaoImpl implements NewsDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<NewsEntity> loadAll() throws DaoException {
		Query query = entityManager.createQuery("from NewsEntity");
		List<NewsEntity> newsList = query.getResultList();
		return newsList;
	}

	@Override
	public NewsEntity loadById(Long id) throws DaoException {
		NewsEntity newsEntity = entityManager.find(NewsEntity.class, id);
		return newsEntity;
	}

	@Override
	public List<NewsEntity> loadByFilter(FilteredItem filteredItem,
			Integer pageNumber, int newsPerPage) throws DaoException {
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();
		String strQuery = QueryHelper.loadOrderedList(filteredItem);
		Query query = entityManager.createNativeQuery(strQuery);
		if (!isAuthorNull) {
			query.setParameter("authorId", authorId);
		}
		if (!isTagNull) {
			query.setParameter("tagIdList", tagIdList);
		}
		int firstIdx = (pageNumber - 1) * newsPerPage;
		query.setFirstResult(firstIdx);
		query.setMaxResults(newsPerPage);
		List<Object> idList = (List<Object>) query.getResultList();

		List<NewsEntity> newsList = new ArrayList<NewsEntity>();
		if (idList == null || idList.isEmpty()) {
			return newsList;
		}
		for (Object row : idList) {
			long id = findIdInRow(row);
			NewsEntity newsEntity = entityManager.find(NewsEntity.class, id);
			newsList.add(newsEntity);
		}
		return newsList;
	}

	private long findIdInRow(Object row) {
		long l;
		if (row instanceof Number) {
			l = ((Number) row).longValue();
		} else {
			// we have two columns
			Object[] objArray = (Object[]) row;
			l = ((Number) (objArray[0])).longValue();
		}
		return l;
	}

	@Override
	public Long create(NewsEntity entity) throws DaoException {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getId();
	}

	@Override
	public void update(NewsEntity entity) throws DaoException {
		entityManager.merge(entity);
	}

	@Override
	public Long loadNextId(FilteredItem filteredItem, Long newsId)
			throws DaoException {
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();

		String strQuery = QueryHelper.loadOffsetId(filteredItem, 1);
		Query query = entityManager.createNativeQuery(strQuery);
		if (!isAuthorNull) {
			query.setParameter("authorId", authorId);
		}
		if (!isTagNull) {
			query.setParameter("tagIdList", tagIdList);
		}
		query.setParameter("newsId", newsId);
		List<BigDecimal> idList = query.getResultList();
		Long nextId = null;
		if (idList != null && idList.size() > 0) {
			nextId = idList.get(0).longValue();
		}
		return nextId;
	}

	@Override
	public Long loadPreviousId(FilteredItem filteredItem, Long newsId)
			throws DaoException {
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();

		String strQuery = QueryHelper.loadOffsetId(filteredItem, -1);
		Query query = entityManager.createNativeQuery(strQuery);
		if (!isAuthorNull) {
			query.setParameter("authorId", authorId);
		}
		if (!isTagNull) {
			query.setParameter("tagIdList", tagIdList);
		}
		query.setParameter("newsId", newsId);
		Long previousId = null;
		List<BigDecimal> idList = query.getResultList();
		if (idList != null && idList.size() > 0) {
			previousId = idList.get(0).longValue();
		}
		return previousId;
	}

	@Override
	public void delete(Long id) throws DaoException {
		NewsEntity entity = entityManager.find(NewsEntity.class, id);
		if (entity != null) {
			entityManager.remove(entity);
		}
	}

	@Override
	public void deleteList(List<Long> newsIdList) throws DaoException {
		Query query = entityManager.createNamedQuery("News.deleteList");
		query.setParameter("idList", newsIdList);
		query.executeUpdate();
	}

	@Override
	public int count(FilteredItem filteredItem) throws DaoException {
		List<Long> tagIdList = filteredItem.getTagIdList();
		Long authorId = filteredItem.getAuthorId();
		boolean isAuthorNull = authorId == null
				|| Long.compare(authorId, 0) == 0;
		boolean isTagNull = tagIdList == null || tagIdList.isEmpty();

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<NewsEntity> root = criteria.from(NewsEntity.class);

		List<Predicate> predicateList = new ArrayList<Predicate>();
		if (!isAuthorNull) {
			Join<NewsEntity, AuthorEntity> author = root.join("author",
					JoinType.INNER);
			Predicate p = builder.in(author.get("id")).value(authorId);
			predicateList.add(p);
		}
		if (!isTagNull) {
			Join<NewsEntity, TagEntity> tags = root
					.join("tags", JoinType.INNER);
			Predicate p = builder.in(tags.get("id")).value(tagIdList);
			predicateList.add(p);
		}
		criteria.where(predicateList.toArray(new Predicate[predicateList.size()]));

		criteria.select(builder.countDistinct(root));
		TypedQuery<Long> typedQuery = entityManager.createQuery(criteria);
		int count = typedQuery.getSingleResult().intValue();
		return count;
	}

}
