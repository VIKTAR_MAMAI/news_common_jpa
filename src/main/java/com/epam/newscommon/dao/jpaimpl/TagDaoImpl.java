package com.epam.newscommon.dao.jpaimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.epam.newscommon.dao.TagDao;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;

@Repository
public class TagDaoImpl implements TagDao {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<TagEntity> loadAll() throws DaoException {
		Query query = entityManager.createQuery("from TagEntity order by tag_name asc");
		return query.getResultList();
	}

	@Override
	public TagEntity loadById(Long id) throws DaoException {
		return entityManager.find(TagEntity.class, id);
	}
	

	@Override
	public List<TagEntity> loadByIdList(List<Long> tagIdList)
			throws DaoException {
		Query query = entityManager.createNamedQuery("Tag.loadByIdList");
		query.setParameter("tagIdList", tagIdList);
		return query.getResultList();
	}

	@Override
	public Long create(TagEntity entity) throws DaoException {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getId();
	}

	@Override
	public void update(TagEntity entity) throws DaoException {
		entityManager.merge(entity);
	}

	@Override
	public void delete(Long id) throws DaoException {
		TagEntity entity = loadById(id);
		if (entity != null) {
			entityManager.remove(entity);
		}
	}

}
