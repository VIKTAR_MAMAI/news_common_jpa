package com.epam.newscommon.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * 
 * mapped with table author
 * 
 */
@Entity
@Table(name = "author")
@NamedQueries({ @NamedQuery(name = "Author.loadActiveAuthors", query = "FROM AuthorEntity a WHERE a.expiredDate is NULL ORDER BY a.name") })
public class AuthorEntity implements Serializable {
	private static final long serialVersionUID = -7256138554405623825L;

	@Id
	@Column(name = "author_id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "author_seq_generator")
	@SequenceGenerator(name = "author_seq_generator", sequenceName = "AUTHOR_SEQ")
	private Long id;
	@Column(name = "name", length = 30, nullable = false)
	private String name;
	@Column(name = "expired")
	private Date expiredDate;
	@Version
	@Column(name = "version")
	private int version;

	public AuthorEntity() {
	}

	public AuthorEntity(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorEntity other = (AuthorEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthorEntity [id=" + id + ", name=" + name + "]";
	}

}
