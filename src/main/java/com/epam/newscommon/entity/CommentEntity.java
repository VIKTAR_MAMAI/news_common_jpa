package com.epam.newscommon.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * mapped with table comments
 * 
 */
@Entity
@Table(name = "comments")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = -1985331543318205729L;

	@Id
	@Column(name = "comment_id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "comment_seq_generator")
	@SequenceGenerator(name = "comment_seq_generator", sequenceName = "COMMENT_SEQ")
	private Long id;
	@Column(name = "comment_text", length = 100, nullable = false)
	private String text;
	@Column(name = "creation_date", nullable = false)
	private Date creationDate;
	@ManyToOne
    @JoinColumn(name="news_id")
	private NewsEntity news;

	public CommentEntity() {
	}

	public CommentEntity(String text) {
		this.text = text;
	}
	
	@PrePersist
	public void prePersist() {
		this.creationDate = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NewsEntity getNews() {
		return news;
	}

	public void setNews(NewsEntity news) {
		this.news = news;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentEntity other = (CommentEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommentEntity [id=" + id + ", text=" + text + ", creationDate=" + creationDate + "]";
	}

}
