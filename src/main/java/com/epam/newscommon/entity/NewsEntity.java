package com.epam.newscommon.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * 
 * mapped with table news
 * 
 */
@Entity
@Table(name = "news")
@NamedQueries({ @NamedQuery(name = "News.deleteList", query = "DELETE FROM NewsEntity n WHERE n.id IN :idList") })
public class NewsEntity implements Serializable {
	private static final long serialVersionUID = 8513291983185889134L;

	@Id
	@Column(name = "news_id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "news_seq_generator")
	@SequenceGenerator(name = "news_seq_generator", sequenceName = "NEWS__SEQ")
	private Long id;
	@Column(name = "short_text")
	private String shortText;
	@Column(name = "full_text")
	private String fullText;
	@Column(name = "title")
	private String title;
	@Column(name = "creation_date", updatable = false)
	private Date creationDate;
	@Column(name = "modification_date")
	private Date modificationDate;
	@Version
	@Column(name = "version")
	private int version;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "news_tag", joinColumns = { @JoinColumn(name = "news_id") }, inverseJoinColumns = { @JoinColumn(name = "tag_id") })
	private List<TagEntity> tags;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "news_author", joinColumns = { @JoinColumn(name = "news_id") }, inverseJoinColumns = { @JoinColumn(name = "author_id") })
	private AuthorEntity author;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "news")
	@OrderBy(value = "creationDate desc")
	private List<CommentEntity> comments;

	public NewsEntity() {
	}

	public NewsEntity(String shortText, String fullText, String title,
			Date creationDate) {
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = creationDate;
	}

	@PrePersist
	public void onPersist() {
		this.modificationDate = this.creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public List<TagEntity> getTags() {
		return tags;
	}

	public void setTags(List<TagEntity> tags) {
		this.tags = tags;
	}

	public List<CommentEntity> getComments() {
		return comments;
	}

	public void setComments(List<CommentEntity> comments) {
		this.comments = comments;
	}

	public AuthorEntity getAuthor() {
		return author;
	}

	public void setAuthor(AuthorEntity author) {
		this.author = author;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsEntity other = (NewsEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsEntity [id=" + id + ", shortText=" + shortText
				+ ", fullText=" + fullText + ", title=" + title
				+ ", creationDate=" + creationDate + ", modificationDate="
				+ modificationDate + "]";
	}

}
