package com.epam.newscommon.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * 
 * mapped with table tag
 * 
 */
@Entity
@Table(name = "tag")
@NamedQueries({ @NamedQuery(name = "Tag.loadByIdList", query = "FROM TagEntity t WHERE t.id in :tagIdList") })
public class TagEntity implements Serializable {
	private static final long serialVersionUID = 9221657837871000144L;

	@Id
	@Column(name = "tag_id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "tag_seq_generator")
	@SequenceGenerator(name = "tag_seq_generator", sequenceName = "TAG_SEQ")
	private Long id;
	@Column(name = "tag_name", length = 30, nullable = false)
	private String name;
	@Version
	@Column(name = "version")
	private int version;

	public TagEntity() {
	}

	public TagEntity(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagEntity other = (TagEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TagEntity [id=" + id + ", name=" + name + "]";
	}

}
