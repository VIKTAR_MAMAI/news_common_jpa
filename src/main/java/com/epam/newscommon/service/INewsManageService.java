package com.epam.newscommon.service;

import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.valueobject.NewsVO;

public interface INewsManageService {

	Long create(NewsVO newsObject) throws ServiceException;

	void update(NewsVO newsObject) throws ServiceException;
}
