package com.epam.newscommon.service;

import java.util.List;

import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.valueobject.FilteredItem;
import com.epam.newscommon.valueobject.NewsPageItem;

public interface INewsService {
	/**
	 * 
	 * @return - full list of news, without tags, author and comments
	 * @throws ServiceException
	 */
	List<NewsEntity> loadAll() throws ServiceException;

	/**
	 * 
	 * @param id
	 *            - unique news identifier
	 * @return - news entity if was found
	 * @throws ServiceException
	 */
	NewsEntity loadById(Long id) throws ServiceException;
	
	/**
	 * 
	 * @param filteredItem - object which is used as a filter
	 * @param pageNumber - number of page with news, displayed on view
	 * @param newsPerPage - count of news displayed on one single page
	 * @return - list of news objects
	 * @throws ServiceException
	 */
	NewsPageItem loadByFilter(FilteredItem filteredItem, Integer pageNumber, int newsPerPage)
			throws ServiceException;

	/**
	 * 
	 * @param filteredItem - object which is used as a filter
	 * @param id - unique news identifier
	 * @return - next news identifier from database according to specified filter
	 * @throws ServiceException
	 */
	Long loadNextId(FilteredItem filteredItem, Long id) throws ServiceException;

	/**
	 * 
	 * @param filteredItem - object which is used as a filter
	 * @param id - unique news identifier
	 * @return - previous news identifier from database according to specified filter
	 * @throws ServiceException
	 */
	Long loadPreviousId(FilteredItem filteredItem, Long id) throws ServiceException;

	/**
	 * 
	 * @param entity - to be created
	 * @return - generated news identifier
	 * @throws ServiceException
	 */
	Long create(NewsEntity entity) throws ServiceException;
	/**
	 * 
	 * @param entity - to be updated
	 * @throws ServiceException
	 */
	void update(NewsEntity entity) throws ServiceException;

	/**
	 * 
	 * @param id
	 *            - unique news identifier
	 * @throws ServiceException
	 */
	void delete(Long id) throws ServiceException;

	/**
	 * @param newsIdList
	 *            - list of news identifiers to be deleted
	 * @throws ServiceException
	 */
	void deleteList(List<Long> newsIdList) throws ServiceException;

	
}
