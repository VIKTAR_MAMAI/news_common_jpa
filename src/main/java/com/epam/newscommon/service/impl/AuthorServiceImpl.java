package com.epam.newscommon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.IAuthorService;

@Service
@Transactional
public class AuthorServiceImpl implements IAuthorService {

	@Autowired
	private AuthorDao authorDao;

	@Override
	public List<AuthorEntity> loadAll() throws ServiceException {
		try {
			List<AuthorEntity> authorList = authorDao.loadAll();
			return authorList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<AuthorEntity> loadActiveAuthors() throws ServiceException {
		try {
			List<AuthorEntity> authorList = authorDao.loadActiveAuthors();
			return authorList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public AuthorEntity loadById(Long id) throws ServiceException {
		try {
			AuthorEntity entity = authorDao.loadById(id);
			return entity;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long create(AuthorEntity entity) throws ServiceException {
		try {
			Long id = authorDao.create(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(AuthorEntity entity) throws ServiceException {
		try {
			authorDao.update(entity);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long authorId) throws ServiceException {
		try {
			authorDao.delete(authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void makeExpired(Long authorId) throws ServiceException {
		try {
			authorDao.makeExpired(authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	

}
