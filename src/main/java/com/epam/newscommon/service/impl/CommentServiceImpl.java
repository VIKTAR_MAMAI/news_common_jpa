package com.epam.newscommon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.CommentDao;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.ICommentService;

@Service
@Transactional
public class CommentServiceImpl implements ICommentService {

	@Autowired
	private CommentDao commentDao;

	@Override
	public List<CommentEntity> loadAll() throws ServiceException {
		try {
			List<CommentEntity> commentList = commentDao.loadAll();
			return commentList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public CommentEntity loadById(Long id) throws ServiceException {
		try {
			CommentEntity entity = commentDao.loadById(id);
			return entity;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long create(CommentEntity entity) throws ServiceException {
		try {
			Long id = commentDao.create(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(CommentEntity entity) throws ServiceException {
		try {
			commentDao.update(entity);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDao.delete(commentId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
