package com.epam.newscommon.service.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.IAuthorService;
import com.epam.newscommon.service.INewsManageService;
import com.epam.newscommon.service.INewsService;
import com.epam.newscommon.service.ITagService;
import com.epam.newscommon.valueobject.NewsVO;

@Service
@Transactional
public class NewsManageServiceImpl implements INewsManageService {

	@Autowired
	private INewsService newsService;
	@Autowired
	private ITagService tagService;
	@Autowired
	private IAuthorService authorService;

	@Override
	public Long create(NewsVO newsObject) throws ServiceException {
		NewsEntity news = newsObject.getNewsEntity();
		List<Long> tagIdList = newsObject.getTagIdList();
		if (tagIdList != null && !tagIdList.isEmpty()) {
			tagIdList.removeAll(Collections.singleton(null));
			List<TagEntity> tagList = tagService.loadByIdList(tagIdList);
			news.setTags(tagList);
		}
		Long authorId = newsObject.getAuthorId();
		AuthorEntity author = authorService.loadById(authorId);
		news.setAuthor(author);
		Long newsId = newsService.create(news);
		return newsId;
	}

	@Override
	public void update(NewsVO newsObject) throws ServiceException {
		NewsEntity news = newsObject.getNewsEntity();
		List<Long> tagIdList = newsObject.getTagIdList();
		if (tagIdList != null && !tagIdList.isEmpty()) {
			tagIdList.removeAll(Collections.singleton(null));
			List<TagEntity> tagList = tagService.loadByIdList(tagIdList);
			news.setTags(tagList);
		}
		Long authorId = newsObject.getAuthorId();
		AuthorEntity author = authorService.loadById(authorId);
		news.setAuthor(author);
		newsService.update(news);
	}

}
