package com.epam.newscommon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.INewsService;
import com.epam.newscommon.valueobject.FilteredItem;
import com.epam.newscommon.valueobject.NewsPageItem;

@Service
@Transactional
public class NewsServiceImpl implements INewsService {
	@Autowired
	private NewsDao newsDao;

	@Override
	public List<NewsEntity> loadAll() throws ServiceException {
		try {
			List<NewsEntity> newsList = newsDao.loadAll();
			return newsList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public NewsEntity loadById(Long id) throws ServiceException {
		try {
			NewsEntity newsEntity = newsDao.loadById(id);
			return newsEntity;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public NewsPageItem loadByFilter(FilteredItem filteredItem,
			Integer pageNumber, int newsPerPage) throws ServiceException {
		if (pageNumber == null) {
			pageNumber = 1;
		}
		try {
			List<NewsEntity> newsList = newsDao.loadByFilter(filteredItem,
					pageNumber, newsPerPage);
			double count = newsDao.count(filteredItem);
			int pageCount = (int) Math.ceil(count / newsPerPage);
			return new NewsPageItem(newsList, pageNumber, pageCount);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long create(NewsEntity entity) throws ServiceException {
		try {
			Long id = newsDao.create(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(NewsEntity entity) throws ServiceException {
		try {
			newsDao.update(entity);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long loadNextId(FilteredItem filteredItem, Long id)
			throws ServiceException {
		try {
			return newsDao.loadNextId(filteredItem, id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long loadPreviousId(FilteredItem filteredItem, Long id)
			throws ServiceException {
		try {
			return newsDao.loadPreviousId(filteredItem, id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			newsDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteList(List<Long> newsIdList) throws ServiceException {
		if (newsIdList == null) {
			throw new ServiceException("No one news was selected to delete!");
		}
		try {
			newsDao.deleteList(newsIdList);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
}
