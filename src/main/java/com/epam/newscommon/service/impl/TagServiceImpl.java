package com.epam.newscommon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.TagDao;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.ITagService;

@Service
@Transactional
public class TagServiceImpl implements ITagService {

	@Autowired
	private TagDao tagDao;

	@Override
	public List<TagEntity> loadAll() throws ServiceException {
		try {
			List<TagEntity> tagList = tagDao.loadAll();
			return tagList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public TagEntity loadById(Long id) throws ServiceException {
		try {
			TagEntity entity = tagDao.loadById(id);
			return entity;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<TagEntity> loadByIdList(List<Long> tagIdList)
			throws ServiceException {
		try {
			List<TagEntity> tagList = tagDao.loadByIdList(tagIdList);
			return tagList;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long create(TagEntity entity) throws ServiceException {
		try {
			Long id = tagDao.create(entity);
			return id;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(TagEntity entity) throws ServiceException {
		try {
			tagDao.update(entity);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long tagId) throws ServiceException {
		try {
			tagDao.delete(tagId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
