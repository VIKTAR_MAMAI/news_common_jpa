package com.epam.newscommon.valueobject;

import java.util.List;

/**
 * 
 * Used as a filter for news. Each news object may have strongly one author and
 * any number of tags (zero also possible)
 * 
 */
public class FilteredItem {

	private final Long authorId;
	private final List<Long> tagIdList;

	public FilteredItem(List<Long> tagIdList, Long authorId) {
		this.authorId = authorId;
		this.tagIdList = tagIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

}
