package com.epam.newscommon.valueobject;

import java.util.List;

import com.epam.newscommon.entity.NewsEntity;

/**
 * <p>
 * value object that stores data about specific list of objects that corresponds
 * to one page on user view
 */
public class NewsPageItem {

	/**
	 * list which will be displayed on one single page
	 */
	private final List<NewsEntity> newsList;
	/**
	 * current page number for which we are storing list of news
	 */
	private final Integer pageNumber;
	/**
	 * total count of pages
	 */
	private final Integer pageCount;

	public NewsPageItem(List<NewsEntity> newsList, Integer pageNumber,
			Integer pageCount) {
		this.newsList = newsList;
		this.pageNumber = pageNumber;
		this.pageCount = pageCount;
	}

	public List<NewsEntity> getNewsList() {
		return newsList;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public Integer getPageCount() {
		return pageCount;
	}
}
