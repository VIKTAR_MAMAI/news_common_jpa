package com.epam.newscommon.valueobject;

import java.util.List;

import com.epam.newscommon.entity.NewsEntity;

public class NewsVO {
	private final NewsEntity newsEntity;
	private final List<Long> tagIdList;
	private final Long authorId;

	public NewsVO(NewsEntity newsEntity, List<Long> tagIdList, Long authorId) {
		this.newsEntity = newsEntity;
		this.tagIdList = tagIdList;
		this.authorId = authorId;
	}

	public NewsEntity getNewsEntity() {
		return newsEntity;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

}
