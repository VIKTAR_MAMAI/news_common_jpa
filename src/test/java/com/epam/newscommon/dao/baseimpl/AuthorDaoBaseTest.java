package com.epam.newscommon.dao.baseimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.AuthorDao;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.exception.DaoException;

@Ignore
@Transactional(propagation=Propagation.REQUIRED)
public class AuthorDaoBaseTest {

	@Autowired
	private AuthorDao authorDao;

	@Test
	public void loadAll() throws DaoException {
		List<AuthorEntity> authorList = authorDao.loadAll();
		assertEquals(20, authorList.size());
	}

	@Test
	public void loadActiveAuthors() throws DaoException {
		List<AuthorEntity> authorList = authorDao.loadActiveAuthors();
		assertEquals(20, authorList.size());
	}

	@Test
	public void loadById() throws DaoException {
		AuthorEntity entity = authorDao.loadById(Long.valueOf(100));
		assertNull(entity);
	}

	@Test
	public void create() throws DaoException {
		AuthorEntity entity = new AuthorEntity();
		entity.setName("valera");
		authorDao.create(entity);
		entity = new AuthorEntity();
		entity.setName("gilbert");
		authorDao.create(entity);
		List<AuthorEntity> authorList = authorDao.loadAll();
		assertEquals(22, authorList.size());
	}

	@Test
	public void update() throws DaoException {
		AuthorEntity entity = new AuthorEntity("gilbert");
		entity.setId(Long.valueOf(5));
		entity.setVersion(1);
		authorDao.update(entity);
		AuthorEntity actualEntity = authorDao.loadById(Long.valueOf(5));
		assertEntityEquals(entity, actualEntity);
	}

	@Test
	public void delete() throws DaoException {
		Long id = Long.valueOf(6);
		authorDao.delete(id);

		AuthorEntity author = authorDao.loadById(id);
		assertNull(author);
	}

	@Test
	public void makeExpired() throws DaoException {
		Long id = Long.valueOf(6);
		authorDao.makeExpired(id);

		AuthorEntity author = authorDao.loadById(id);
		assertNotNull(author.getExpiredDate());
	}

	private void assertEntityEquals(AuthorEntity expected, AuthorEntity actual) {
		assertEquals(expected.getName(), actual.getName());
	}
}
