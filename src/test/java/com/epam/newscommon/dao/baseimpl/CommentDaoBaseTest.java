package com.epam.newscommon.dao.baseimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.CommentDao;
import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;

@Ignore
@Transactional(propagation=Propagation.REQUIRED)
public class CommentDaoBaseTest {

	@Autowired
	private CommentDao commentDao;
	
	@Autowired
	private NewsDao newsDao;

	@Test
	public void loadAll() throws DaoException {
		List<CommentEntity> commentList = commentDao.loadAll();
		assertEquals(20, commentList.size());
	}

	@Test
	public void loadById() throws DaoException, ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = format.parse("2010-12-19 00:00:00");

		CommentEntity expectedEntity = new CommentEntity("british museum - my favourite one");
		expectedEntity.setCreationDate(date);
		NewsEntity newsEntity = new NewsEntity();
		newsEntity.setId(Long.valueOf(5));
		expectedEntity.setNews(newsEntity);
		CommentEntity actualEntity = commentDao.loadById(Long.valueOf(5));
		assertEntityEquals(expectedEntity, actualEntity);
	}

	@Test
	public void create() throws DaoException {
		CommentEntity entity = new CommentEntity();
		entity.setText("be aware");
		NewsEntity news = newsDao.loadById(Long.valueOf(1));
		entity.setNews(news);
		Long id = commentDao.create(entity);
		assertNotNull(id);
		List<CommentEntity> actualList = commentDao.loadAll();
		assertEquals(21, actualList.size());
	}

	@Test
	public void update() throws DaoException {
		NewsEntity news = newsDao.loadById(Long.valueOf(1));
		CommentEntity entity = new CommentEntity("tv taboo banned");
		entity.setId(Long.valueOf(11));
		entity.setCreationDate(new Date());
		entity.setNews(news);
		commentDao.update(entity);
		CommentEntity actualEntity = commentDao.loadById(Long.valueOf(11));
		assertEntityEquals(entity, actualEntity);
	}

	@Test
	public void delete() throws DaoException {
		commentDao.delete(Long.valueOf(11));
		commentDao.delete(Long.valueOf(14));
		List<CommentEntity> commentList = commentDao.loadAll();
		assertEquals(18, commentList.size());
	}

	private void assertEntityEquals(CommentEntity expected, CommentEntity actual) {
		assertEquals(expected.getText(), actual.getText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getNews().getId(), actual.getNews().getId());
	}
}
