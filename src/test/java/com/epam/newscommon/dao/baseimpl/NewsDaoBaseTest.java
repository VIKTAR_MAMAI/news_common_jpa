package com.epam.newscommon.dao.baseimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.NewsDao;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.valueobject.FilteredItem;

@Ignore
@Transactional(propagation = Propagation.REQUIRED)
public abstract class NewsDaoBaseTest {

	@Autowired
	private NewsDao newsDao;

	@Test
	public void loadAll() throws DaoException {
		List<NewsEntity> newsList = newsDao.loadAll();
		assertEquals(20, newsList.size());
	}

	@Test
	public void loadById() throws DaoException {
		NewsEntity actualEntity = new NewsEntity(
				"South Africa need to score 13 more runs",
				"Chameera gets the hook after two rather forgettable overs, and fellow right-arm seamer Thisara Perera is on as Sri Lanka's sixth bowler, looking to make amends for his third-ball duck.",
				"Cricket superbowl", Date.valueOf("2009-07-07"));
		actualEntity.setModificationDate(Date.valueOf("2009-07-08"));

		NewsEntity expectedEntity = newsDao.loadById(Long.valueOf(2));
		assertEntityEquals(actualEntity, expectedEntity);

		List<CommentEntity> comments = expectedEntity.getComments();
		assertEquals(3, comments.size());

		AuthorEntity author = expectedEntity.getAuthor();
		assertEquals("Boris", author.getName());

		List<TagEntity> tags = expectedEntity.getTags();
		assertEquals(2, tags.size());
	}

	@Test
	public void loadByFilter() throws DaoException {
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 4 }));
		Long authorId = Long.valueOf(2);
		List<NewsEntity> newsList = newsDao.loadByFilter(new FilteredItem(
				tagIdList, authorId), 1, 30);
		assertEquals(2, newsList.size());

		authorId = null;
		newsList = newsDao.loadByFilter(new FilteredItem(tagIdList, authorId),
				1, 30);
		assertEquals(5, newsList.size());

		authorId = Long.valueOf(2);
		tagIdList = null;
		newsList = newsDao.loadByFilter(new FilteredItem(tagIdList, authorId),
				1, 30);
		assertEquals(3, newsList.size());

		authorId = null;
		newsList = newsDao.loadByFilter(new FilteredItem(tagIdList, authorId),
				1, 30);
		assertEquals(20, newsList.size());
	}

	@Test
	public void loadByFilterAll() throws DaoException {
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
				19, 20 }));
		Long authorId = null;
		int page = 1;
		List<NewsEntity> newsList = newsDao.loadByFilter(new FilteredItem(
				tagIdList, authorId), page++, 6);
		assertEquals(6, newsList.size());

		newsList = newsDao.loadByFilter(new FilteredItem(tagIdList, authorId),
				page++, 6);
		assertEquals(4, newsList.size());

		newsList = newsDao.loadByFilter(new FilteredItem(tagIdList, authorId),
				page++, 6);
		assertEquals(0, newsList.size());

	}

	@Test
	public void count() throws DaoException {
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 4 }));
		Long authorId = Long.valueOf(2);
		int count = newsDao.count(new FilteredItem(tagIdList, authorId));
		assertEquals(2, count);

		authorId = null;
		count = newsDao.count(new FilteredItem(tagIdList, authorId));
		assertEquals(5, count);

		authorId = Long.valueOf(2);
		tagIdList = null;
		count = newsDao.count(new FilteredItem(tagIdList, authorId));
		assertEquals(3, count);

		authorId = null;
		count = newsDao.count(new FilteredItem(tagIdList, authorId));
		assertEquals(20, count);
	}

	@Test
	public void loadNextId() throws DaoException {
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 6 }));
		Long authorId = Long.valueOf(2);
		Long newsId = Long.valueOf(2);
		FilteredItem filteredItem = new FilteredItem(tagIdList, authorId);
		Long nextId = newsDao.loadNextId(filteredItem, newsId);
		assertEquals(Long.valueOf(6), nextId);

		tagIdList = Arrays.asList(ArrayUtils
				.toObject(new long[] { 1, 3, 5, 6 }));
		authorId = null;
		newsId = Long.valueOf(1);
		filteredItem = new FilteredItem(tagIdList, authorId);
		nextId = newsDao.loadNextId(filteredItem, newsId);
		assertEquals(Long.valueOf(3), nextId);

		tagIdList = null;
		authorId = Long.valueOf(6);
		newsId = Long.valueOf(6);
		filteredItem = new FilteredItem(tagIdList, authorId);
		nextId = newsDao.loadNextId(filteredItem, newsId);
		assertNull(nextId);

		tagIdList = null;
		authorId = null;
		newsId = Long.valueOf(11);
		filteredItem = new FilteredItem(tagIdList, authorId);
		nextId = newsDao.loadNextId(filteredItem, newsId);
		assertEquals(Long.valueOf(5), nextId);
	}

	@Test
	public void loadPreviousId() throws DaoException {
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 6 }));
		Long authorId = Long.valueOf(2);
		Long newsId = Long.valueOf(2);
		FilteredItem filteredItem = new FilteredItem(tagIdList, authorId);
		Long previousId = newsDao.loadPreviousId(filteredItem, newsId);
		assertEquals(Long.valueOf(5), previousId);

		tagIdList = Arrays.asList(ArrayUtils
				.toObject(new long[] { 1, 3, 5, 6 }));
		authorId = null;
		newsId = Long.valueOf(1);
		filteredItem = new FilteredItem(tagIdList, authorId);
		previousId = newsDao.loadPreviousId(filteredItem, newsId);
		assertEquals(Long.valueOf(6), previousId);

		tagIdList = null;
		authorId = Long.valueOf(2);
		newsId = Long.valueOf(2);
		filteredItem = new FilteredItem(tagIdList, authorId);
		previousId = newsDao.loadPreviousId(filteredItem, newsId);
		assertEquals(Long.valueOf(5), previousId);

		tagIdList = null;
		authorId = null;
		newsId = Long.valueOf(11);
		filteredItem = new FilteredItem(tagIdList, authorId);
		previousId = newsDao.loadPreviousId(filteredItem, newsId);
		assertNull(previousId);
	}

	@Test
	public void create() throws DaoException {
		NewsEntity expectedEntity = new NewsEntity("Colla battle blamed",
				"Colla battle blamed", "Colla battle blamed",
				Date.valueOf("2012-03-14"));
		/*
		 * expectedEntity.setTags(generateTagList()); AuthorEntity author =
		 * generateAuthorList().get(0); expectedEntity.setAuthor(author);
		 * expectedEntity.setComments(new ArrayList<CommentEntity>());
		 */
		Long id = newsDao.create(expectedEntity);
		assertNotNull(id);

		NewsEntity actualEntity = newsDao.loadById(id);
		assertEntityEquals(expectedEntity, actualEntity);
	}

	@Test
	public void update() throws DaoException {
		Long newsId = Long.valueOf(1);
		NewsEntity expectedEntity = newsDao.loadById(newsId);
		/*
		 * expectedEntity.setTags(generateTagList()); AuthorEntity author =
		 * generateAuthorList().get(1); expectedEntity.setAuthor(author);
		 */
		newsDao.update(expectedEntity);

		NewsEntity actualEntity = newsDao.loadById(newsId);
		assertEntityEquals(expectedEntity, actualEntity);
	}

	@Test
	public void delete() throws DaoException {
		newsDao.delete(Long.valueOf(2));

		NewsEntity entity = newsDao.loadById(Long.valueOf(2));

		assertNull(entity);
	}

	@Test
	public void deleteList() throws DaoException {
		List<Long> newsIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				2, 3 }));
		newsDao.deleteList(newsIdList);

		NewsEntity entity = newsDao.loadById(Long.valueOf(2));
		assertNull(entity);

		assertEquals(18, newsDao.loadAll().size());
	}

	// additional methods for assertions

	private void assertEntityEquals(NewsEntity expectedEntity,
			NewsEntity actualEntity) {

		assertEquals(expectedEntity.getTitle(), actualEntity.getTitle());
		assertEquals(expectedEntity.getShortText(), actualEntity.getShortText());
		assertEquals(expectedEntity.getFullText(), actualEntity.getFullText());
		assertEquals(expectedEntity.getCreationDate(),
				actualEntity.getCreationDate());
		assertEquals(expectedEntity.getModificationDate(),
				actualEntity.getModificationDate());
	}

	private List<TagEntity> generateTagList() {
		List<TagEntity> tagList = new ArrayList<TagEntity>();
		tagList.add(new TagEntity("sun"));
		tagList.add(new TagEntity("rain"));
		tagList.add(new TagEntity("thunder"));
		return tagList;
	}

	private List<AuthorEntity> generateAuthorList() {
		List<AuthorEntity> authorList = new ArrayList<AuthorEntity>();
		authorList.add(new AuthorEntity("kitaev"));
		authorList.add(new AuthorEntity("molenkov"));
		authorList.add(new AuthorEntity("ruslanov"));
		return authorList;
	}

	private List<CommentEntity> generateCommentList() {
		List<CommentEntity> commentList = new ArrayList<CommentEntity>();
		commentList.add(new CommentEntity("satisfied!!!"));
		commentList.add(new CommentEntity("best team"));
		commentList.add(new CommentEntity("fantastic game"));
		return commentList;
	}

}
