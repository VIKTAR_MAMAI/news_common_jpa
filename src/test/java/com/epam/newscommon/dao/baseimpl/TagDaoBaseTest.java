package com.epam.newscommon.dao.baseimpl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.dao.TagDao;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;

@Ignore
@Transactional(propagation = Propagation.REQUIRED)
public class TagDaoBaseTest {

	@Autowired
	private TagDao tagDao;

	@Test
	public void loadAll() throws DaoException {
		List<TagEntity> tagList = tagDao.loadAll();
		assertEquals(20, tagList.size());
	}

	@Test
	public void loadById() throws DaoException {
		TagEntity entity = new TagEntity("oracle");
		TagEntity actualEntity = tagDao.loadById(Long.valueOf(7));
		assertEntityEquals(entity, actualEntity);
	}

	@Test
	public void loadByIdList() throws DaoException {
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 4 }));
		List<TagEntity> tagList = tagDao.loadByIdList(tagIdList);
		assertEquals( 4, tagList.size());
	}

	@Test
	public void create() throws DaoException {
		TagEntity entity = new TagEntity();
		entity.setName("brilliant");
		tagDao.create(entity);
		List<TagEntity> actualList = tagDao.loadAll();
		assertEquals(21, actualList.size());
	}

	@Test
	public void update() throws DaoException {
		TagEntity entity = new TagEntity("powerful");
		entity.setId(Long.valueOf(7));
		entity.setVersion(1);
		tagDao.update(entity);
		TagEntity actualEntity = tagDao.loadById(Long.valueOf(7));
		assertEntityEquals(entity, actualEntity);
	}

	@Test
	public void delete() throws DaoException {
		tagDao.delete(Long.valueOf(10));
		List<TagEntity> actualList = tagDao.loadAll();
		assertEquals(19, actualList.size());
	}

	@Test
	public void deleteNull() throws DaoException {
		tagDao.delete(Long.valueOf(100));
		List<TagEntity> actualList = tagDao.loadAll();
		assertEquals(20, actualList.size());
	}

	private void assertEntityEquals(TagEntity expected, TagEntity actual) {
		Assert.assertEquals(expected.getName(), actual.getName());
	}
}
