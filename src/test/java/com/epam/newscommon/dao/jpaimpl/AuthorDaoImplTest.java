package com.epam.newscommon.dao.jpaimpl;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newscommon.dao.baseimpl.AuthorDaoBaseTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:jpaContextTest.xml")
@TestExecutionListeners(listeners = { DbUnitTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class })
@DatabaseSetup(value = { "/dbunit/author-data.xml"})
public class AuthorDaoImplTest extends AuthorDaoBaseTest{
}
