package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.dao.hibernateimpl.AuthorDaoImpl;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {
	@InjectMocks
	private AuthorServiceImpl authorService;

	@Mock
	private AuthorDaoImpl authorDao;

	@Test
	public void loadAll() throws ServiceException, DaoException {
		doReturn(generateAuthorList()).when(authorDao).loadAll();
		List<AuthorEntity> actualTagList = authorService.loadAll();
		verify(authorDao, times(1)).loadAll();
		assertEquals(3, actualTagList.size());
	}

	@Test(expected = ServiceException.class)
	public void loadAllFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).loadAll();
		authorService.loadAll();
	}

	@Test
	public void loadActiveAuthors() throws ServiceException, DaoException {
		doReturn(generateAuthorList()).when(authorDao).loadActiveAuthors();
		List<AuthorEntity> actualTagList = authorService.loadActiveAuthors();
		verify(authorDao, times(1)).loadActiveAuthors();
		assertEquals(3, actualTagList.size());
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		AuthorEntity entity = new AuthorEntity();
		String authorName = "molenkov";
		entity.setName(authorName);
		doReturn(entity).when(authorDao).loadById(Long.valueOf(4));
		AuthorEntity actualEntity = authorService.loadById(Long.valueOf(4));
		verify(authorDao, times(1)).loadById(anyLong());
		assertEquals(authorName, actualEntity.getName());
	}

	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).loadById(anyLong());
		authorService.loadById(Long.valueOf(4));
	}

	@Test
	public void create() throws ServiceException, DaoException {
		AuthorEntity entity = new AuthorEntity();
		entity.setName("petrenko");
		Long id = authorService.create(entity);
		assertNotNull(id);
	}

	@Test(expected = ServiceException.class)
	public void createFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).create(any(AuthorEntity.class));
		AuthorEntity entity = new AuthorEntity();
		entity.setName("petrenko");
		authorService.create(entity);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		AuthorEntity entity = new AuthorEntity("stepanov");
		entity.setId(Long.valueOf(3));
		authorService.update(entity);
		verify(authorDao, atLeastOnce()).update(any(AuthorEntity.class));
	}

	@Test
	public void delete() throws ServiceException, DaoException {
		authorService.delete(Long.valueOf(2));
		authorService.delete(Long.valueOf(4));
		verify(authorDao, times(2)).delete(anyLong());
	}

	@Test(expected = ServiceException.class)
	public void deleteFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(authorDao).delete(anyLong());
		authorService.delete(Long.valueOf(4));
	}

	@Test
	public void makeExpired() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(2);
		authorService.makeExpired(authorId);
		verify(authorDao).makeExpired(authorId);
	}
	
	private List<AuthorEntity> generateAuthorList() {
		ArrayList<AuthorEntity> authorList = new ArrayList<AuthorEntity>() {
			{
				this.add(new AuthorEntity("kitaev"));
				this.add(new AuthorEntity("molenkov"));
				this.add(new AuthorEntity("ruslanov"));
			}
		};
		return authorList;
	}
}
