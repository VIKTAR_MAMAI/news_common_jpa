package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.dao.hibernateimpl.CommentDaoImpl;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	@InjectMocks
	private CommentServiceImpl commentService;

	@Mock
	private CommentDaoImpl commentDao;

	@Test
	public void loadAll() throws ServiceException, DaoException {
		doReturn(generateCommentList()).when(commentDao).loadAll();
		List<CommentEntity> actualList = commentService.loadAll();
		assertEquals(3, actualList.size());
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		doReturn(null).when(commentDao).loadById(Long.valueOf(100));
		CommentEntity entity = commentService.loadById(Long.valueOf(100));
		assertNull(entity);
	}

	@Test
	public void create() throws ServiceException, DaoException {
		CommentEntity entity = new CommentEntity("satisfied!!!");
		entity.setId(Long.valueOf(100));
		commentService.create(entity);
		entity = new CommentEntity("favourite game");
		commentService.create(entity);

		verify(commentDao, times(2)).create(any(CommentEntity.class));
	}

	@Test
	public void update() throws ServiceException, DaoException {
		CommentEntity entity = new CommentEntity("satisfied!!!");
		entity.setId(Long.valueOf(2));
		commentService.update(entity);
		verify(commentDao).update(entity);
	}

	@Test
	public void delete() throws ServiceException, DaoException {
		commentService.delete(Long.valueOf(2));
		commentService.delete(Long.valueOf(6));

		verify(commentDao, atLeast(2)).delete(anyLong());
	}

	@Test(expected = ServiceException.class)
	public void deleteFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(commentDao).delete(anyLong());
		commentService.delete(Long.valueOf(50));
		commentService.delete(Long.valueOf(100));
	}

	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(commentDao).loadById(anyLong());
		commentService.loadById(Long.valueOf(2));
	}

	private List<CommentEntity> generateCommentList() {
		List<CommentEntity> commentList = new ArrayList<CommentEntity>() {
			{
				this.add(new CommentEntity("satisfied!!!"));
				this.add(new CommentEntity("best team"));
				this.add(new CommentEntity("fantastic game"));
			}
		};
		return commentList;
	}
}
