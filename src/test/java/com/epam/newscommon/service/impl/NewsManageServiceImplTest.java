package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.valueobject.NewsVO;

@RunWith(MockitoJUnitRunner.class)
public class NewsManageServiceImplTest {

	@InjectMocks
	private static NewsManageServiceImpl newsManageService;

	@Mock
	private TagServiceImpl tagService;
	@Mock
	private AuthorServiceImpl authorService;
	@Mock
	private NewsServiceImpl newsService;

	@Test
	public void createWithTagsAndAuthor() throws ServiceException, DaoException {
		NewsEntity actualEntity = new NewsEntity(
				"South Africa need to score 13 more runs",
				"Chameera gets the hook after two rather forgettable overs, and fellow right-arm seamer Thisara Perera is on as Sri Lanka's sixth bowler, looking to make amends for his third-ball duck.",
				"Cricket superbowl", Date.valueOf("2009-07-07"));
		actualEntity.setModificationDate(Date.valueOf("2009-07-08"));
		Long expectedId = Long.valueOf(14);
		doReturn(expectedId).when(newsService).create(actualEntity);
		List<Long> unmodifiableList = Arrays.asList(ArrayUtils
				.toObject(new long[] { 7, 8, 9, 10, 11 }));
		List<Long> tagIdList = new ArrayList<Long>(unmodifiableList);
		tagIdList.add(null);
		Long authorId = Long.valueOf(12);
		NewsVO newsObject = new NewsVO(actualEntity, tagIdList, authorId);
		Long id = newsManageService.create(newsObject);

		verify(authorService).loadById(authorId);
		verify(tagService).loadByIdList(tagIdList);
		verify(newsService).create(actualEntity);
		
		assertEquals(expectedId, id);

	}

	@Test
	public void updateWithTagsAndAuthor() throws ServiceException, DaoException {
		NewsEntity actualEntity = new NewsEntity(
				"South Africa need to score 13 more runs",
				"Chameera gets the hook after two rather forgettable overs, and fellow right-arm seamer Thisara Perera is on as Sri Lanka's sixth bowler, looking to make amends for his third-ball duck.",
				"Cricket superbowl", null);
		actualEntity.setModificationDate(Date.valueOf("2009-07-08"));
		actualEntity.setId(Long.valueOf(13));
		actualEntity.setVersion(1);
		List<Long> unmodifiableList = Arrays.asList(ArrayUtils
				.toObject(new long[] { 7, 8, 9, 10, 11 }));
		List<Long> tagIdList = new ArrayList<Long>(unmodifiableList);
		tagIdList.add(null);
		Long authorId = Long.valueOf(12);

		NewsVO newsObject = new NewsVO(actualEntity, tagIdList, authorId);
		newsManageService.update(newsObject);

		verify(authorService).loadById(authorId);
		verify(tagService).loadByIdList(tagIdList);
		verify(newsService).update(actualEntity);
	}
}
