package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.dao.hibernateimpl.NewsDaoImpl;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.valueobject.FilteredItem;
import com.epam.newscommon.valueobject.NewsPageItem;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	@InjectMocks
	private static NewsServiceImpl newsService;

	@Mock
	private NewsDaoImpl newsDao;

	@Test
	public void loadAll() throws ServiceException, DaoException {
		doReturn(generateList()).when(newsDao).loadAll();

		assertNotNull(newsService.loadAll());
		assertEquals(3, newsService.loadAll().size());
	}

	@Test(expected = ServiceException.class)
	public void loadAllFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(newsDao).loadAll();
		newsService.loadAll();
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		NewsEntity news1 = new NewsEntity("liverpool with arsenal",
				"liverpool plays with arsenal in quarters", "football",
				Date.valueOf("2004-12-01"));
		doReturn(news1).when(newsDao).loadById(Long.valueOf(1));
		NewsEntity news2 = new NewsEntity("major resign in russia",
				"russian chief athletics resigns", "athletics",
				Date.valueOf("2014-09-25"));
		doReturn(news2).when(newsDao).loadById(Long.valueOf(3));

		assertEquals(news1, newsService.loadById(Long.valueOf(1)));
		assertEquals(news2, newsService.loadById(Long.valueOf(3)));
		assertNull(newsService.loadById(Long.valueOf(2)));
	}

	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(newsDao).loadById(anyLong());
		newsService.loadById(Long.valueOf(3));
	}

	@Test
	public void loadByFilter() throws ServiceException, DaoException {
		int newsPerPage = 7;
		int pageNumber = 1;
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 6 }));
		FilteredItem filteredItem = new FilteredItem(tagIdList, authorId);
		doReturn(32).when(newsDao).count(filteredItem);
		List<NewsEntity> newsList = generateList();
		doReturn(newsList).when(newsDao).loadByFilter(filteredItem, 1,
				newsPerPage);

		NewsPageItem item = newsService.loadByFilter(filteredItem, null,
				newsPerPage);
		assertEquals(Integer.valueOf(5), item.getPageCount());
		assertEquals(Integer.valueOf(pageNumber), item.getPageNumber());
		assertEquals(newsList.size(), item.getNewsList().size());
	}

	@Test
	public void loadNextId() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		FilteredItem filteredItem = new FilteredItem(tagIdList, authorId);
		newsService.loadNextId(filteredItem, newsId);
		verify(newsDao).loadNextId(filteredItem, newsId);
	}

	@Test(expected = ServiceException.class)
	public void loadNextIdFail() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		FilteredItem filteredItem = new FilteredItem(tagIdList, authorId);
		doThrow(DaoException.class).when(newsDao).loadNextId(filteredItem,
				newsId);
		newsService.loadNextId(filteredItem, newsId);
	}

	@Test
	public void loadPreviousId() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		FilteredItem filteredItem = new FilteredItem(tagIdList, authorId);
		newsService.loadPreviousId(filteredItem, newsId);
		verify(newsDao).loadPreviousId(filteredItem, newsId);
	}

	@Test(expected = ServiceException.class)
	public void loadPreviousIdFail() throws ServiceException, DaoException {
		Long authorId = Long.valueOf(3);
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 3, 5, 6 }));
		Long newsId = Long.valueOf(5);
		FilteredItem filteredItem = new FilteredItem(tagIdList, authorId);
		doThrow(DaoException.class).when(newsDao).loadPreviousId(filteredItem,
				newsId);
		newsService.loadPreviousId(filteredItem, newsId);
	}

	@Test
	public void create() throws ServiceException, DaoException {
		NewsEntity entity = new NewsEntity(
				"Recovery operation resumes",
				"A search and recovery operation has resumed in the southern French Alps after Tuesday's crash of a Germanwings plane with 150 people on board",
				"Air crash", Date.valueOf("2007-11-06"));
		Long id = newsService.create(entity);
		assertNotNull(id);
		verify(newsDao).create(entity);
	}

	

	@Test(expected = ServiceException.class)
	public void createFail() throws ServiceException, DaoException {
		NewsEntity entity = new NewsEntity(
				"Recovery operation resumes",
				"A search and recovery operation has resumed in the southern French Alps after Tuesday's crash of a Germanwings plane with 150 people on board",
				"Air crash", Date.valueOf("2007-11-06"));
		doThrow(DaoException.class).when(newsDao).create(any(NewsEntity.class));
		newsService.create(entity);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		NewsEntity news = new NewsEntity("scotland and italy",
				"scotland plays with italy in final", "rugby",
				Date.valueOf("2014-10-23"));
		news.setTitle("italy beats scotland surprisingly");
		newsService.update(news);
		verify(newsDao).update(news);
	}

	

	@Test
	public void delete() throws ServiceException, DaoException {
		newsService.delete(Long.valueOf(5));
		newsService.delete(Long.valueOf(1));
		newsService.delete(Long.valueOf(102));

		verify(newsDao, times(3)).delete(anyLong());
	}

	@Test
	public void deleteList() throws ServiceException, DaoException {
		List<Long> newsIdList = Arrays.asList((long) 3, (long) 4);
		newsService.deleteList(newsIdList);
		verify(newsDao).deleteList(newsIdList);
	}

	@Test(expected = ServiceException.class)
	public void deleteListFailNull() throws ServiceException, DaoException {
		newsService.deleteList(null);
	}

	@Test(expected = ServiceException.class)
	public void deleteListFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(newsDao).deleteList(
				Mockito.anyListOf(Long.class));
		List<Long> idList = Arrays.asList(ArrayUtils.toObject(new long[] { 1,
				3, 5, 6 }));
		newsService.deleteList(idList);
	}

	private List<NewsEntity> generateList() {
		List<NewsEntity> newsList = new ArrayList<NewsEntity>();
		newsList.add(new NewsEntity("liverpool with arsenal",
				"liverpool plays with arsenal in quarters", "football", Date
						.valueOf("2004-12-01")));
		newsList.add(new NewsEntity("scotland and italy",
				"scotland plays with italy in final", "rugby", Date
						.valueOf("2014-10-23")));
		newsList.add(new NewsEntity("major resign in russia",
				"russian chief athletics resigns", "athletics", Date
						.valueOf("2014-09-25")));
		return newsList;
	}
}
