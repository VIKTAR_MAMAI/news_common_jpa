package com.epam.newscommon.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newscommon.dao.hibernateimpl.TagDaoImpl;
import com.epam.newscommon.entity.TagEntity;
import com.epam.newscommon.exception.DaoException;
import com.epam.newscommon.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
	@InjectMocks
	private TagServiceImpl tagService;

	@Mock
	private TagDaoImpl tagDao;

	private List<TagEntity> generateTagList() {
		ArrayList<TagEntity> tagList = new ArrayList<TagEntity>() {
			{
				this.add(new TagEntity("sun"));
				this.add(new TagEntity("rain"));
				this.add(new TagEntity("thunder"));
			}
		};
		return tagList;
	}

	@Test
	public void loadAll() throws ServiceException, DaoException {
		doReturn(generateTagList()).when(tagDao).loadAll();
		List<TagEntity> actualTagList = tagService.loadAll();
		verify(tagDao, times(1)).loadAll();
		assertEquals(3, actualTagList.size());
	}

	@Test(expected = ServiceException.class)
	public void loadAllFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(tagDao).loadAll();
		tagService.loadAll();
	}

	@Test
	public void loadById() throws ServiceException, DaoException {
		TagEntity entity = generateTagList().get(1); // id = 7
		doReturn(entity).when(tagDao).loadById(Long.valueOf(7));
		TagEntity actualEntity = tagService.loadById(Long.valueOf(7));
		verify(tagDao, times(1)).loadById(anyLong());
		assertEntityEquals(actualEntity, entity);
	}

	@Test(expected = ServiceException.class)
	public void loadByIdFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(tagDao).loadById(anyLong());
		tagService.loadById(Long.valueOf(4));
	}

	@Test
	public void loadByIdList() throws ServiceException, DaoException {
		List<TagEntity> tagList = generateTagList();
		List<Long> tagIdList = Arrays.asList(ArrayUtils.toObject(new long[] {
				1, 2, 3 }));
		doReturn(tagList).when(tagDao).loadByIdList(tagIdList);
		List<TagEntity> actualList = tagService.loadByIdList(tagIdList);
		assertEquals(3, actualList.size());
	}

	@Test
	public void create() throws ServiceException, DaoException {
		TagEntity entity = new TagEntity();
		entity.setName("color");
		Long id = tagService.create(entity);
		assertNotNull(id);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		verifyZeroInteractions(tagDao);
		TagEntity entity = new TagEntity("color");
		entity.setId(Long.valueOf(7));
		tagService.update(entity);
		verify(tagDao, atLeastOnce()).update(any(TagEntity.class));
	}

	@Test(expected = ServiceException.class)
	public void updateFail() throws ServiceException, DaoException {
		TagEntity entity = new TagEntity("sunshine");
		entity.setId(Long.valueOf(101));
		doThrow(DaoException.class).when(tagDao).update(entity);
		tagService.update(entity);
	}

	@Test
	public void delete() throws ServiceException, DaoException {
		tagService.delete(Long.valueOf(7));
		verify(tagDao).delete(anyLong());
	}

	@Test(expected = ServiceException.class)
	public void deleteFail() throws ServiceException, DaoException {
		doThrow(DaoException.class).when(tagDao).delete(anyLong());
		tagService.delete(Long.valueOf(4));
	}

	private void assertEntityEquals(TagEntity entity, TagEntity actualEntity) {
		assertEquals(entity.getName(), actualEntity.getName());
	}
}
